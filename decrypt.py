import binascii

cipher_text = int(input("Enter cipher text : "))
d,n = tuple(int(x) for x in input("Enter public key : ").split(','))
decrypted_text = pow(cipher_text, d, n)

print('d : ' + str(d))
print('n : ' + str(n))
print('cipher text : ' + str(cipher_text))
print('decrypted text : ' + str(decrypted_text))
print('message : ' + binascii.unhexlify(hex(decrypted_text)[2:]).decode())